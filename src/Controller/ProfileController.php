<?php

namespace App\Controller;

use App\Form\ImageType;
use App\Form\ProfileType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController
{
    /**
     * @Route("/profile", name="profile",options={"expose"=true})
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $EM = $this->getDoctrine()->getManager();
        $formIMG = $this->createForm(ImageType::class, $this->getUser());
        $formIMG->handleRequest($request);
        if($formIMG->isSubmitted() && $formIMG->isValid()) {
            $user = $formIMG->getData();
            $user->setUpdatedAt(new \DateTime());
            $EM->persist($user);
            $EM->flush();
            return new Response('ok');
        }


        $form = $this->createForm(ProfileType::class,$this->getUser());
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $user= $form->getData();
            $EM->persist($user);
            $EM->flush();
            return new Response('ok');
        }

        return $this->render('profile/index.html.twig', [
            'controller_name' => 'ProfileController',
            'form' => $form->createView(),
            'formIMG'=> $formIMG->createView()
        ]);
    }
}
