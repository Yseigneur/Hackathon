<?php

namespace App\Controller\Back;

use App\Entity\Project;
use App\Entity\User;
use App\Form\ProjectType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProjectController
 */
class ProjectController extends AbstractController
{
    /**
     * @Route("/project/{id}", name="project", methods={"GET","POST"})
     */
    public function index(Request $request,User $user): Response
    {
        $form = $this->createForm(ProjectType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $project = $form->getData();
            $project->setUser($user);
            $project->setStatus('waiting');
            $EM = $this->getDoctrine()->getManager();
            $EM->persist($project);
            $EM->flush();
            return $this->redirectToRoute('user_index');
        }

        return $this->render('project.html.twig',[
            'form' => $form->createView()
        ]);
    }
}
