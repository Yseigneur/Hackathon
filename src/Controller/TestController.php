<?php

namespace App\Controller;

use App\Entity\Badge;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    /**
     * @Route("/test", name="test")
     */
    public function index()
    {
        $em= $this->getDoctrine()->getRepository(Badge::class);
        return $this->render('test/index.html.twig', [
            'controller_name' => 'TestController',
        ]);
    }
    /**
     * @Route("/test_end", name="test_end")
     */
    public function end()
    {
        $em=$this->getDoctrine()->getManager();
        $user=$this->getUser();
        $user->setRoles(['ROLE_USER']);
        $em->persist($user);
        $em->flush();
        return $this->redirectToRoute('index');
    }

    /**
     * @Route("/test_dev", name="test_dev")
     */
    public function dev()
    {
        return $this->render('test/dev.html.twig', [
            'controller_name' => 'TestController',
        ]);
    }
    /**
     * @Route("/submit_dev", name="submit_dev")
     */
    public function submit_dev()
    {
        $badge= new Badge();
        $badge->setName("Dev");
        $badge->setGrade('100');
        $badge->setUser($this->getUser());
        $EM = $this->getDoctrine()->getManager();
        $EM->persist($badge);
        $EM->flush();
        return $this->redirectToRoute('test');
    }
    /**
     * @Route("/test_market", name="test_market")
     */
    public function market()
    {
        return $this->render('test/market.html.twig', [
            'controller_name' => 'TestController',
        ]);
    }
    /**
     * @Route("/test_des", name="test_des")
     */
    public function des()
    {
        return $this->render('test/des.html.twig', [
            'controller_name' => 'TestController',
        ]);
    }
    /**
     * @Route("/test_chef", name="test_chef")
     */
    public function chef()
    {
        return $this->render('test/chef.html.twig', [
            'controller_name' => 'TestController',
        ]);
    }

}
