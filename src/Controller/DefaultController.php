<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        if($this->get('security.authorization_checker')->isGranted('ROLE_TEST')) {
            return $this->redirectToRoute('test');
        }
        if($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
           $count= count($this->getUser()->getProjects());
        } else {
            $count =0;
        }


            return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'count' => $count
        ]);
    }
}
