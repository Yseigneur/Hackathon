<?php

namespace App\Controller;

use App\Entity\Project;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProjectsController extends AbstractController
{
    /**
     * @Route("/projects", name="projects")
     */
    public function index()
    {
        $EM= $this->getDoctrine()->getRepository(Project::class);

        return $this->render('projects.html.twig', [
            'controller_name' => 'ProjectsController',
            'projects_wait' => $EM->findBy(['User'=>$this->getUser(),'status' =>'waiting']),
            'projects_ok' => $EM->findBy(['User'=>$this->getUser(),'status' =>'ok'])
        ]);
    }

    /**
     * @Route("/project_accept/{id}", name="project_accept")
     */
    public function accept(Project $project)
    {
        $EM= $this->getDoctrine()->getManager();
        $project->setStatus('ok');
        $EM->persist($project);
        $EM->flush();
        return $this->redirectToRoute('projects');
    }

    /**
     * @Route("/project_refuse/{id}", name="project_refuse")
     */
    public function refuse(Project $project)
    {
        $EM= $this->getDoctrine()->getManager();
        $EM->remove($project);
        $EM->flush();
        return $this->redirectToRoute('projects');
    }
}
