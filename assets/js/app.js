/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import '../scss/app.scss';

require('../../public/assets/vendor/fonts/ionicons.css');
require('../../public/assets/vendor/css/bootstrap.css');
require('../../public/assets/vendor/css/appwork.css');
require('../../public/assets/vendor/css/colors.css');
require('../../public/assets/vendor/css/uikit.css');
require('../../public/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css');
require('../../public/assets/vendor/libs/spinkit/spinkit.css');
require('../../public/assets/vendor/libs/toastr/toastr.css');
require('../../public/assets/vendor/libs/sweetalert2/sweetalert2.css');

require('../../public/assets/vendor/libs/bootstrap-material-datetimepicker/bootstrap-material-datetimepicker.css');
require('../../public/assets/vendor/libs/bootstrap-daterangepicker/bootstrap-daterangepicker.css');
require('../../public/assets/vendor/libs/bootstrap-datepicker/bootstrap-datepicker.css');
require('../../public/assets/vendor/libs/flatpickr/flatpickr.css');
require('../../public/assets/vendor/libs/timepicker/timepicker.css');
require('../../public/assets/vendor/libs/minicolors/minicolors.css');
require('../../public/assets/vendor/fonts/fontawesome.css');
require('../scss/app.scss');

require('../../public/assets/vendor/libs/bootstrap-markdown/bootstrap-markdown.css');

require('../../public/assets/vendor/libs/bootstrap-slider/bootstrap-slider.css');
require('../../public/assets/vendor/libs/nouislider/nouislider.css');



// Need jQuery? Install it with "yarn add jquery", then uncomment to require it. require('../');
//import $ from 'jquery';
const $ = require('jquery');

window.$ = global.$ = global.jQuery = $;

const $contactButton = $('#contactButton')
$contactButton.click(e => {
    e.preventDefault()
    $('#contactForm').slideDown();
    $contactButton.slideUp();

})


require('../../public/assets/vendor/js/polyfills.js');
require('../../public/assets/vendor/js/layout-helpers.js');
require('../../public/assets/vendor/libs/popper/popper.js');
require('../../public/assets/vendor/js/bootstrap.js');
require('../../public/assets/vendor/js/sidenav.js');
require('../../public/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js');
require('../../public/assets/vendor/libs/block-ui/block-ui.js');
require('../../public/assets/vendor/libs/toastr/toastr.js');
require('../../public/assets/vendor/libs/sweetalert2/sweetalert2.js');

require('../../public/assets/vendor/libs/moment/moment.js');
require('../../public/assets/vendor/libs/bootstrap-datepicker/bootstrap-datepicker.js');
require('../../public/assets/vendor/libs/bootstrap-material-datetimepicker/bootstrap-material-datetimepicker.js');
require('../../public/assets/vendor/libs/bootstrap-daterangepicker/bootstrap-daterangepicker.js');
require('../../public/assets/vendor/libs/flatpickr/flatpickr.js');
require('../../public/assets/vendor/libs/minicolors/minicolors.js');
require('../../public/assets/vendor/libs/sortablejs/sortable.js');

require('../../public/assets/vendor/libs/markdown/markdown.js');
require('../../public/assets/vendor/libs/bootstrap-markdown/bootstrap-markdown.js');

require('../../public/assets/vendor/libs/bootstrap-slider/bootstrap-slider.js');
require('../../public/assets/vendor/libs/nouislider/nouislider.js');




console.log('Hello Webpack Encore! Edit me in assets/js/app.js');
