# Hackathon

Les agents du web



# Prerequisites!

  

-  Symfony 5 
 
- php 7.3
  
- localhost database
  
- Yarn installed
  
- composer installed




### Installation



Clone our project 



update database

```sh

$ php bin/console d:s:u --dump-sql

$ php bin/console d:s:u --force
```



### Launch



start php server


```sh

$ symfony server:start

```



go to this url : https://127.0.0.1:8000/



### Made By



Akrim Mehdi

Seigneur Yannick 